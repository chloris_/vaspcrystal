# vaspcrystal

Performs non-recursive status analysis of VASP calculations in subdirectories
of the current working directory, and analysis, export and plotting of
calculation data. Data is exported to subdirectory `vaspcrystal` in the current
working directory. Plotting is performed by Gnuplot via system calls.

Usage:
`vaspcrystal [task]`

Valid tasks are `status` (the default one) and `analyze`. Task `status` will
analyze subdirectories with calculations and print their summaries to standard
output. Task analyze `analyze` will create a subdirectory `vaspcrystal` and
write more thorough analysis data there, including plots.

`vaspcrystal version` will display the version of the program.

## How to compile

This utility is written in Crystal and requires the Crystal compiler. The build
process using `shards` dependency manager is very simple:
```bash
shards build --release --progress
```
This command produces executable `vaspcrystal` in directory `bin`.

If your Crystal instalation lacks `shards`, Crystal compiler may be invoked
directly:

```bash
crystal build src/main.cr -o vaspcrystal --release --progress
```
This command should produce an executable named `vaspcrystal`.

Code documentation can be built using the following command:
```bash
crystal doc
```
