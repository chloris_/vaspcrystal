require "spec"
require "../src/calculation_data.cr"

describe CalculationData do
    it "is initialized" do
        data = CalculationData.new
        data.system_name.should eq ""
        data.energies.size.should eq 0
        data.volumes.size.should eq 0
        data.calculation_complete.should eq false
    end
end
