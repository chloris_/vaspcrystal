require "spec"
require "../src/calculation_extractor.cr"

# Required accuracy for energies
ENERGY_DELTA = 1e-6
# Required accuracy for volumes
VOLUME_DELTA = 1e-6

describe CalculationExtractor do
    it "test case no-acn-rp" do
        # Expected data
        exp_name = "no-acn-rp"
        exp_energies = [
            -3289.26599397, -2828.17752985, -3331.69772403, -3389.84044233,
            -3397.57285449, -3402.34041090, -3403.23747910, -3403.47616376,
            -3403.68223874, -3403.86108713, -3404.04451193, -3404.27268831,
            -3404.35744809, -3404.38648723, -3404.40536152, -3404.41464496,
            -3404.42305189, -3404.43219755, -3404.44007478, -3404.44773163,
            -3404.46116089, -3404.48471428, -3404.50353941, -3404.51684108,
            -3404.53289192 ]
        exp_volumes = [
            5661.280, 5661.280, 5661.280, 5661.280, 5661.280, 5661.280,
            5661.280, 5661.280, 5661.280, 5661.280, 5661.280, 5661.280,
            5661.280, 5661.280, 5661.280, 5661.280, 5661.280, 5661.280,
            5661.280, 5661.280, 5661.280, 5661.280, 5661.280, 5661.280,
            5661.280, 5661.280 ]
        exp_complete = true

        # Extract data
        path = "spec/test_cases/no-acn-rp"
        extractor = CalculationExtractor.new(path)
        data = extractor.extract_data

        # Compare results
        data.system_name.should eq exp_name
        data.energies.size.should eq exp_energies.size
        data.volumes.size.should eq exp_volumes.size
        data.calculation_complete.should eq exp_complete

        exp_energies.each_index do |i|
            data.energies[i].should be_close(exp_energies[i], ENERGY_DELTA)
        end
        exp_volumes.each_index do |i|
            data.volumes[i].should be_close(exp_volumes[i], VOLUME_DELTA)
        end
    end
end
