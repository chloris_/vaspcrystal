require "spec"
require "../src/calculation_status.cr"
require "../src/calculation_data.cr"
require "../src/calculation_extractor.cr"

# Required precision
DELTA = 1e-6

describe CalculationStatus do
    it "analyzes calculation data" do
        # Expected values
        exp_system_name = "wtf"
        exp_steps_performed = 4
        exp_initial_energy = 100.0
        exp_final_energy = 40.4
        exp_last_delta = -6.9
        exp_calculation_complete = true
        exp_analysis_complete = true

        # Calculation test data
        energies = [100.0, 52.9, 47.3, 40.4]
        data = CalculationData.new
        data.system_name = exp_system_name
        data.energies += energies
        # Ignore volumes
        data.calculation_complete = exp_calculation_complete

        # Analyze calculation data
        status = CalculationStatus.new(data)

        # Test results
        status.analysis_complete.should eq exp_analysis_complete
        status.steps_performed.should eq exp_steps_performed
        status.initial_energy.should eq exp_initial_energy
        status.final_energy.should eq exp_final_energy
        status.last_delta.should be_close(exp_last_delta, DELTA)
        status.calculation_complete.should eq exp_calculation_complete
        status.system_name.should eq exp_system_name
    end

    it "test case no-acn-rp" do
        # Expected data
        exp_system_name = "no-acn-rp"
        exp_steps_performed = 25
        exp_initial_energy =  -3289.26599397
        exp_final_energy = -3404.53289192
        exp_last_delta = -0.016051
        exp_calculation_complete = true
        exp_analysis_complete = true

        # Extract data
        path = "spec/test_cases/no-acn-rp"
        extractor = CalculationExtractor.new(path)
        data = extractor.extract_data

        # Analyze calculation data
        status = CalculationStatus.new(data)

        # Test results
        status.analysis_complete.should eq exp_analysis_complete
        status.steps_performed.should eq exp_steps_performed
        status.initial_energy.should be_close(exp_initial_energy, DELTA)
        status.final_energy.should be_close(exp_final_energy, DELTA)
        status.last_delta.should be_close(exp_last_delta, DELTA)
        status.calculation_complete.should eq exp_calculation_complete
        status.system_name.should eq exp_system_name
    end
    
end
