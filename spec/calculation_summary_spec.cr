require "spec"
require "../src/calculation_summary.cr"

describe CalculationSummary do
    it "extracts summary from calculation data" do
        # Expected data
        exp_name = "wtf"
        exp_complete = true
        exp_steps = 4
        exp_initial_e = 133.7
        exp_final_e = 404.0
        exp_initial_v = 1000.0
        exp_final_v = 1111.1

        # Sample calculation data
        data = CalculationData.new
        data.system_name = exp_name
        data.calculation_complete = exp_complete
        data.energies += [133.7, 134.5, 375.9, 404.0]
        data.volumes += [1000.0, 1002.5, 1009.2, 1470.4, 1111.1]

        # Prepare summary
        summary = CalculationSummary.new(data)

        # Test data
        summary.system_name.should eq exp_name
        summary.calculation_complete.should eq exp_complete
        summary.steps_performed.should eq exp_steps
        summary.initial_energy.should eq exp_initial_e
        summary.final_energy.should eq exp_final_e
        summary.initial_volume.should eq exp_initial_v
        summary.final_volume.should eq exp_final_v
    end
end
