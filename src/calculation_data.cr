#
# Stores calcualtion data extracted from calculation output files.
#
class CalculationData
    # Name of the calculation
    property system_name
    # List of calculated energies
    property energies
    # List of calculated unit cell volumes
    property volumes
    # Whether calculation was indicated to be complete
    property calculation_complete

    #
    # Initializes an empty CalculationData object.
    #
    def initialize
        @system_name = ""
        @energies = [] of Float64
        @volumes = [] of Float64
        @calculation_complete = false
    end
end 
