require "./calculation_data.cr"

#
# Extracts calculation data from a single directory containing files with
# calculation results.
#
class CalculationExtractor 
    # Path to the directory containing calculation files
    getter dir_path

    # Common regular expressions used for data extraction
    @@re_system_title = /^\s*SYSTEM =\s*(\S+)\s*$/
    @@re_energy = /^\s*energy  without entropy\s*=\s*([+-]?[\d\.]+)\s+/
    @@re_volume = /^\s*volume of cell :\s*([\d\.]+)\s*$/

    # Seek offset used to check the end of the OUTCAR file for indication
    # of calculation being complete
    CALC_COMPLETE_SEEK_OFFSET = -1300
    # Substring present when the calculation is complete
    CALC_COMPLETE_STRING = "reached required accuracy"
    # Substring present in lines with energy to be extracted
    ENERGY_SUBSTRING = "energy  without"
    # Substring present in lines with volume to be extracted
    VOLUME_SUBSTRING = "volume of cell"

    #
    # Initializes a new CalculationExtractor with path of the directory from
    # which the data is to be extracted.
    #
    def initialize(dir_path : String)
        @dir_path = dir_path
    end

    #
    # Extracts calculation data from calculation files in directory @dir_path.
    # Returns a CalculationData object with extracted data.
    #
    def extract_data : CalculationData
        data = CalculationData.new
        read_outcar(data)
        return data
    end

    #
    # Reads file OUTCAR in the calculation directory and extracts data in the
    # CalculationData object passed as the argument. The object is not
    # reinitialized prior to data extraction.
    #
    private def read_outcar(calc_data : CalculationData)
        path = File.join(@dir_path, "OUTCAR")
        system_tag_found = false

        begin
            File.open(path, "r") do |file|
                file.each_line do |line|
                    if system_tag_found
                        # Seek energies and volumes
                        line = line.lstrip
                        
                        # Check for presence of energy first
                        # First use a less expensive check than regex
                        if line.starts_with?(ENERGY_SUBSTRING)
                            match = @@re_energy.match(line)
                            unless match.nil?
                                # If there was exactly one capture
                                if match.size == 2
                                    begin
                                        energy = Float64.new(match[1])
                                        calc_data.energies << energy
                                        next
                                    rescue ArgumentError
                                        raise "Could not parse energy " \
                                              "'#{match[1]}' in line '#{line}'"
                                    end
                                else
                                    raise "Unexpected energy regex match:\n" \
                                          "'#{line}' -> #{match.inspect}"
                                end
                            end
                        end
                        
                        # Energy not found, check for volume
                        if line.starts_with?(VOLUME_SUBSTRING)
                            match = @@re_volume.match(line)
                            unless match.nil?
                                # If there was exactly one capture
                                if match.size == 2
                                    begin
                                        volume = Float64.new(match[1])
                                        calc_data.volumes << volume
                                        next
                                    rescue ArgumentError
                                        raise "Could not parse volume " \
                                              "'#{match[1]}' in line '#{line}'"
                                    end
                                else
                                    raise "Unexpected volume regex match:\n" \
                                          "'#{line}' -> #{match.inspect}"
                                end
                            end
                        end
                    else
                        # Seek SYSTEM tag
                        match = @@re_system_title.match(line)
                        if match.nil?
                            next
                        end
                        if match.size == 2
                            calc_data.system_name = match[1]
                            system_tag_found = true
                        else
                            raise "Unexpected system tag regex match:\n" \
                                  "'#{line}' -> #{match.inspect}"
                        end
                    end
                end

                # When file is read check for indication of calculation being
                # complete at the end of the file (if the file is long enough)
                if file.size > CALC_COMPLETE_SEEK_OFFSET.abs
                    begin
                        file.seek(CALC_COMPLETE_SEEK_OFFSET, IO::Seek::End)
                        loop do
                            begin
                                line = file.read_line
                                if line.includes?(CALC_COMPLETE_STRING)
                                    calc_data.calculation_complete = true
                                    break
                                end
                            rescue IO::EOFError
                                break
                            end
                        end
                    rescue e : Exception
                        puts "Error while looking for calculation complete " \
                             "indication in file '#{path}':\n#{e}"
                    end
                end
            end
        rescue e : Exception
            puts "Error while extracting data from file '#{path}':\n#{e}"
        end
    end
end
