#
# Determines calculation status from data of a single calculation and provides
# method for printing calculation status lines.
#
class CalculationStatus
    # Whether data analysis was successful
    getter analysis_complete
    # Number of calculation steps performed
    getter steps_performed
    # First energy of the calculation
    getter initial_energy
    # Last energy of the calculation
    getter final_energy
    # Difference betwen last and second-last energies
    getter last_delta
    # Whether calculation is complete
    getter calculation_complete
    # Name of the calculation
    getter system_name

    # Format string for status line
    STATUS_LINE_FORMAT = "%-18.18s | %4i steps  E = %9.3f eV  "\
                         "delta %11.6f eV  %-6.6s"
    # Format string for status line with failed analysis
    FAIL_LINE_FORMAT = "%-18.18s | < no data >"
    
    #
    # Initializes the object and performs status analysis of calculation data
    # passed as argument.
    #
    def initialize(calc_data : CalculationData)
        @calc_data = calc_data

        # Calculation status variables
        @analysis_complete = false
        @system_name = ""
        @steps_performed = 0
        @initial_energy = 0.0
        @final_energy = 0.0
        @second_last_energy = 0.0
        @last_delta = 0.0
        @calculation_complete = false

        # Perform data analysis
        @analysis_complete = analyze_data
    end

    #
    # Performs analysis of the data that was passed to this object.
    # Results are saved to instance variables. Returns true if analysis was
    # carried out successfully or false otherwise.
    #
    private def analyze_data : Bool
        if @calc_data.system_name.empty?
            raise "Calculation data without system name!"
        end

        @system_name = @calc_data.system_name
        @steps_performed = @calc_data.energies.size
        if @steps_performed >= 1
            @initial_energy = @calc_data.energies[0]
            @final_energy = @calc_data.energies[-1]
        end
        if @steps_performed >= 2
            @second_last_energy = @calc_data.energies[-2]
            @last_delta = @final_energy - @second_last_energy
        end
        @calculation_complete = @calc_data.calculation_complete

        if @steps_performed < 1
            return false
        else
            return true
        end
    end

    #
    # Prints a status line with data derived from provided calculation data
    # to standard output.
    #
    def print_status_line
        if @analysis_complete
            status_flag = ""
            status_flag = "(done)" if @calculation_complete

            puts STATUS_LINE_FORMAT % [@system_name, @steps_performed,\
                 @final_energy, @last_delta, status_flag]
        else
            puts FAIL_LINE_FORMAT % @system_name
        end
    end
end
