require "./calculation_data.cr"

#
# Contains only key properties of a calculation. These properties may be stored
# for entire
#
class CalculationSummary
    @system_name : String
    @calculation_complete : Bool
    @steps_performed : Int32

    # Name of the calculation
    getter system_name
    # Whether calculation was completed
    getter calculation_complete
    # Number of calculation steps performed
    getter steps_performed
    # First energy in the calculation
    getter initial_energy
    # Last energy in the calculation
    getter final_energy
    # First volume in the calculation
    getter initial_volume
    # Last volume in the calculation
    getter final_volume

    #
    # Extracts summary data from full calculation data, passed as the parameter.
    #
    def initialize(calc_data : CalculationData)
        @system_name = calc_data.system_name
        @calculation_complete = calc_data.calculation_complete
        @steps_performed = calc_data.energies.size
        @initial_energy = 0.0
        @final_energy = 0.0
        @initial_volume = 0.0
        @final_volume = 0.0

        unless calc_data.energies.empty?
            @initial_energy = calc_data.energies.first
            @final_energy = calc_data.energies.last
        end

        unless calc_data.volumes.empty?
            @initial_volume = calc_data.volumes.first
            @final_volume = calc_data.volumes.last
        end
    end

end
