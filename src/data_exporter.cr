#
# Provides methods to export data from CalculationData object to files. Files
# are written to a subdirectory inside designated output directory.
# Subdirectory is named after calculation title.
#
class DataExporter
    # Path to the subdirectory the data is exported to
    getter subdir_path
    # Whether energies were exported
    getter energies_exported
    # Whether volumes were exported
    getter volumes_exported
    # Whether Gnuplot script was written
    getter gnuplot_script_written

    # Format string for energies
    ENERGY_FORMAT = "%16.8f"
    # Format string for volumes
    VOLUME_FORMAT = "%10.3f"
    # Gnuplot script file name
    GNUPLOT_GPI = "gnuplot.gpi"
    # Energies data export file name
    ENERGIES_DAT = "energies.dat"
    # Energies plot file name
    ENERGIES_PLOT = "energies.png"
    # Volumes data export file name
    VOLUMES_DAT = "volumes.dat"
    # Volumes plot file name
    VOLUMES_PLOT = "volumes.png"
    
    #
    # Initializes the object with reference to calculation data and determines
    # path to subdirectory and data files for data export.
    #
    # Parameters:
    # * calc_data: CalculationData object to source data from
    # * output_dir: directory inside which subdirectory with exported data
    #               will be created
    #
    def initialize(calc_data : CalculationData, output_dir : String)
        @calc_data = calc_data
        @output_dir = output_dir

        # Determine subdirectory path
        if @calc_data.system_name.empty?
            raise "Missing calculation name during data export!"
        end
        @subdir_path = File.join(@output_dir, @calc_data.system_name)

        # Determine output file paths
        @gpi_path = File.join(@subdir_path, GNUPLOT_GPI)
        @energies_plot_path = File.join(@subdir_path, ENERGIES_PLOT)
        @energies_dat_path = File.join(@subdir_path, ENERGIES_DAT)
        @volumes_plot_path = File.join(@subdir_path, VOLUMES_PLOT)
        @volumes_dat_path = File.join(@subdir_path, VOLUMES_DAT)

        # Data export flags (set when data is exported)
        @energies_exported = false
        @volumes_exported = false
        @gnuplot_script_written = false
    end

    #
    # Creates subdirectory inside designated output directory, writes data
    # files into it and creates a Gnuplot script to plot the data.
    #
    def export_data
        lazy_create_subdir
        write_energies
        write_volumes
        write_gnuplot_script
    end

    #
    # Creates output directory and data export subdirectory, if they don't
    # exist. Raises if creating directories fails.
    #
    private def lazy_create_subdir
        unless Dir.exists?(@output_dir)
            begin
                Dir.mkdir(@output_dir)
            rescue Exception
                raise "Failed to create output directory '#{@output_dir}'"
            end
        end
        unless Dir.exists?(@subdir_path)
            begin
                Dir.mkdir(@subdir_path)
            rescue Exception
                raise "Failed to create export subdirectory '#{@subdir_path}'"
            end
        end
    end

    #
    # Creates file ENERGIES_DAT inside the output subdirectory and writes
    # formatted list of energies to it, if the energy list is not empty.
    #
    private def write_energies
        unless @calc_data.energies.empty?
            begin
                File.open(@energies_dat_path, "w") do |file|
                    @calc_data.energies.each do |energy|
                        file.puts(ENERGY_FORMAT % energy)
                    end
                end
                @energies_exported = true
            rescue e : Exception
                puts "Error while exporting energies for system " \
                     "'#{@calc_data.system_name}':\n#{e}"
            end
        end
    end

    #
    # Creates file VOLUMES_DAT inside the output subdirectory and writes
    # formatted list of volumes to it, if the volume list is not empty.
    #
    private def write_volumes
        unless @calc_data.volumes.empty?
            begin
                File.open(@volumes_dat_path, "w") do |file|
                    @calc_data.volumes.each do |volume|
                        file.puts(VOLUME_FORMAT % volume)
                    end
                end
                @volumes_exported = true
            rescue e : Exception
                puts "Error while exporting volumes for system " \
                     "'#{@calc_data.system_name}':\n#{e}"
            end
        end
    end

    #
    # Writes file GNUPLOT_GPI to the output subdirectory. It contains a script
    # for Gnuplot which plots energies and volumes to image files in the
    # subdirectory. Separate parts of plot instructions are only written if
    # the data to be plotted was already exported. If the data object contains
    # no data, the script is not written.
    #
    private def write_gnuplot_script
        if @calc_data.energies.empty? || @calc_data.volumes.empty?
            puts "No data in system '#{@calc_data.system_name}'. "\
                 "Gnuplot script not written."
            return
        end

        script_preamble = "set terminal pngcairo size 800,500 enhanced\n"
        script_energies = \
            "set output \"#{@energies_plot_path}\"\n"\
            "set xlabel \"Calculation step\"\n"\
            "set ylabel \"Energy [eV]\"\n"\
            "plot \"#{@energies_dat_path}\" u ($0+1):1 w l title " \
            "\"#{@calc_data.system_name}\"\n"
        script_volumes = \
            "set output \"#{@volumes_plot_path}\"\n"\
            "set xlabel \"Calculation step\"\n"\
            "set ylabel \"Volume [Å^3]\"\n"\
            "plot \"#{@volumes_dat_path}\" u ($0):1 w l title "\
            "'#{@calc_data.system_name}'\n"

        begin
            File.open(@gpi_path, "w") do |file|
                file.puts(script_preamble)
                if @energies_exported
                    file.puts(script_energies)
                end
                if @volumes_exported
                    file.puts(script_volumes)
                end
            end
            @gnuplot_script_written = true
        rescue e : Exception
            puts "Error while writing Gnuplot script '#{@gpi_path}':\n#{e}"
        end
    end

    #
    # Executes Gnuplot with created Gnuplot script as parameter, if the script
    # was already written by this object. Returns true, if Gnuplot exited
    # successfully, and false otherwise or if Gnuplot script was not written.
    #
    def run_gnuplot : Bool
        if @gnuplot_script_written
            return system("gnuplot \"#{@gpi_path}\"")
        else
            return false
        end
    end
end
