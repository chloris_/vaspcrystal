# Program entry point

require "./vaspcrystal.cr"

# Parse arguments
task = "status"
if ARGV.size > 1
    STDERR.puts "This program accepts at most one argument.\n"
    Vaspcrystal.print_usage
    exit(-1)
end
if ARGV.size == 1
    arg = ARGV[0].strip

    if ["version", "--version"].includes?(arg)
        puts "Vaspcrystal version #{Vaspcrystal::VERSION} "\
             "(#{Vaspcrystal::DATE})"
        exit()
    end

    if Vaspcrystal::VALID_TASKS.includes?(arg)
        task = arg
    else
        STDERR.puts "Invalid argument '#{arg}'!\n"
        Vaspcrystal.print_usage
        exit(-1)
    end
end

case task
when "status"
    Vaspcrystal.report_calculation_status
when "analyze"
    Vaspcrystal.analyze_subdirs
else
    raise "Unknown task '#{task}'!"
end

