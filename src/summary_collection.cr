require "./calculation_summary.cr"

#
# Collects calculation summaries and provides methods for compiling them into
# files meant to be inspected by users.
#
class SummaryCollection
    # Format string for header of the summary file
    SUMMARY_HEADER_FORMAT = "%-26.26s %-5.5s %13.13s %13.13s "\
                            "%13.13s %13.13s %6.6s"
    # Format string for the summary of a single calculation
    SUMMARY_LINE_FORMAT = "%-26.26s %5i %10.3f eV %10.3f eV "\
                          "%10.3f A³ %10.3f A³ %6.6s"
    # File name of the summary data file
    SUMMARY_DAT = "summary.dat"

    #
    # Initializes a new object with empty calculation summary collection and
    # the path to the output directory, in which the summary file should be
    # written.
    #
    def initialize(output_dir : String)
        @output_dir = output_dir
        @collection = [] of CalculationSummary
    end

    #
    # Adds a calculation summary to the collection
    #
    def <<(summary : CalculationSummary) : self
        @collection << summary
        return self
    end

    #
    # Sorts the collection of CalculationSummary object by system names.
    #
    private def sort_collection
        @collection.sort! { |a, b| a.system_name <=> b.system_name }
    end

    #
    # Writes summary based on calculation summaries presently in the
    # collection to the file SUMMARY_DAT in the output directory.
    #
    def write_summary()
        path = File.join(@output_dir, SUMMARY_DAT)
        sort_collection
        header = SUMMARY_HEADER_FORMAT % ["System name", "Steps", \
                 "Initial E", "Final E", "Initial V", "Final V", \
                 "Compl?"]

        begin
            File.open(path, "w") do |file|
                file.puts(header)
                file.puts("#{"-" * header.size}")
                @collection.each do |s|
                    file.puts(SUMMARY_LINE_FORMAT % \
                               [s.system_name,
                                s.steps_performed,
                                s.initial_energy,
                                s.final_energy,
                                s.initial_volume,
                                s.final_volume,
                                s.calculation_complete.to_s])
                end
            end
        rescue e : Exception
            puts "Error while writing summary file '#{path}':\n#{e}"
        end
    end
    
end
