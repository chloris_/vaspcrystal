require "./summary_collection.cr"
require "./calculation_summary.cr"
require "./calculation_extractor.cr"
require "./data_exporter.cr"
require "./calculation_status.cr"

#
# Provides high-level methods for the functionality of this program.
#
module Vaspcrystal
    # Version of Vaspcrystal
    VERSION = "1.0.3"
    # Date of this version of Vaspcrystal
    DATE = "2021-03-26"

    # Files that are required to be in a calculation directory
    REQUIRED_FILES = ["OUTCAR", "INCAR"]
    # Path to the output directory
    OUTPUT_DIR = File.join(Dir.current, "vaspcrystal")
    # Valid tasks that can be passed as commmand line arguments
    VALID_TASKS = ["analyze", "status"]

    #
    # Checks if the directory has required files for analysis and they
    # are readable. If they are, true is returned, and false otherwise.
    #
    private def self.has_required_files?(dir_path : String) : Bool
        if File.directory?(dir_path)
            entries = Dir.children(dir_path)
            REQUIRED_FILES.each do |file|
                if entries.includes?(file)
                    path = File.join(dir_path, file)
                    if File.directory?(path)
                        return false
                    end
                    unless File.readable?(path)
                        return false
                    end
                else
                    return false
                end
            end
            return true
        else
            raise "'#{dir_path}' does not exist or is not a directory"
        end
    end

    #
    # Performs a non-recursive analysis of calculations in subdirectories of
    # the current working directory. Data is exported to OUTPUT_DIR.
    #
    def self.analyze_subdirs
        root = Dir.new(Dir.current)
        summaries = SummaryCollection.new(OUTPUT_DIR)

        root.each_child do |entry|
            if File.directory?(entry)
                dir_path = File.join(root.path, entry)
                if has_required_files?(dir_path)
                    extractor = CalculationExtractor.new(dir_path)
                    puts ":: Extracting data from '#{dir_path}'"                    
                    data = extractor.extract_data
                    summaries << CalculationSummary.new(data)
                    exporter = DataExporter.new(data, OUTPUT_DIR)
                    puts ":: Exporting data to '#{exporter.subdir_path}'"
                    exporter.export_data
                    if exporter.gnuplot_script_written
                        puts ":: Plotting data for system '#{data.system_name}'"
                        exporter.run_gnuplot
                    end
                end
            end
        end

        puts ":: Writing summary file 'summary.dat'"
        summaries.write_summary
    end

    #
    # Analyzes calculations in subdirectories of the current working directory
    # and prints a status line for each calculation.
    #
    def self.report_calculation_status
        root = Dir.new(Dir.current)
        entries = [] of String

        # Prepare a sorted list of directories to be processed
        root.each_child do |entry|
            if File.directory?(entry)
                dir_path = File.join(root.path, entry)
                if has_required_files?(dir_path)
                    entries << dir_path
                end
            end
        end
        entries.sort!

        # Process the directories
        entries.each do |dir|
            extractor = CalculationExtractor.new(dir)
            data = extractor.extract_data
            status = CalculationStatus.new(data)
            status.print_status_line
        end
    end

    #
    # Prints short usage summary for the program to standard output.
    #
    def self.print_usage
        puts "Usage:\n"\
             "  vaspcrystal [task]\n\n"\
             "Valid tasks are:\n"\
             "* analyze\n"\
             "* status\n"\
             "Default task is 'status'."
    end
end
